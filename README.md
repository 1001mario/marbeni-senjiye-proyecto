# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This repository is for storing a Java project simulating a meter with respective lines that can pass through some stations. We also store the scripts and files necesaries for clean and compile the proyect by itself to be executed, it is a maven project, so it should work if you have Java installed in your computer.

* Version

The version of the project is still snapshot 0.0.1, so we are working on the project right now but it will soon be released.

### How do I get set up? ###

* Summary of set up

The Java classes are in the src/main/java/marbeni-senjiye_metro folder with contains all the .java files, those classes have the implementation already and you can use that classes by yourself without any problem, but you need to read before the javadoc and documentation about how to use them!

There are also test in the project in the other folder: src/test/java/marbeni-senjiye_Metro, those are the junit classes which shows how to use the classes and also how not to use!

* Configuration

You need to have Java 7 installed in your computer and operating system to run the tests and programs that use the classes. If you haven't got Jdk installed on your team, you need to [install jdk 7](http://www.oracle.com/technetwork/es/java/javase/downloads/jdk7-downloads-1880260.html).

You can use an IDE (like Eclipse) to run the aplication and the project and if you download the project of this repository, eclipse project is compiled and it is not necessary to compile again, but if you clean the project with ant, you will need to compile the classes and tests with the ant script (eclipse also have an option to run and compile the ant script) to run the application and tests. 

So make sure you have jdk 7 installed on your team and ant also if you are going to clean, compile, build or run the project, both are necessary because there are sometimes you will need to clean, generate javadoc or make other relative tasks.

* Dependencies

The Maven project depends on junit 3.8 and there are other dependency about easymocks, which are important to do the test cases with mock objects to help the program running without implementations yet.

* How to run tests

The test are all in the suite class: AllTests.java contained in the Java project marbeni-senjiye in the master branch. It contains the white and black box tests that prove the three java classes of our meter: Metro, Linea and Estacion.

Those three classes are being proved each of them by testing classes that contains bad and good ways to use the respective class, so if you want to know how the class works, you just can see the methods of those tests and see what you can do with the classes or what you cant.

As said before, you can run those test separately or you can run the AllTest suite class that runs all of the tests (it's more easy run one class but as you want).

* Deployment instructions

You can do just two differents things to run the Java project: one is using the eclipse IDE importing the Maven project (you need to have Maven in Eclipse of course), then you can run the AllTests class in the package src/test/java/marbeni-senjiye_Metro of the Java project imported. That class is a suite which contains all test (including the white and black cage) and you can see them.

The other option is run tests using ant in the linux console, you just go to the route of the proyect and inside the project there is an ant script called build.xml, then you can do some tasks: clean, compile, generate javadoc or run tests, if you need to run all the tests you will use the next command in the console: 'ant' and this shows you the information of the init, build, compilation and the run of all tests of the project, setting a new folder named junit in the proyect that contains the information of the tests execution.

You can use other ant commands like: ant ejecutar, that runs all tests like the command explained before, because 'ant' just have the run of tests as default.
you can clean with 'ant limpiar', compile with 'ant compilar' and you can generate the documentation of the classes and tests with 'ant documentar'.

If you want to run only the White box tests, you can use the 'ant ejecutarTestEnAislamiento', or you want the black ones you can use the command 'ant ejecutarTestsTDD'.

You can also see the information about all tests with 'ant obtenerInformeCobertura' which first runs the tests of the project and then it creates the folder junit.

### Contribution guidelines ###

* Writing tests

The tests are written by the admins of the repository:
   Mario Benito => 1001mario (Bitbucket username)
   Senmao Ji Ye => Senmao (Bitbucket username)

* Code review

We just worked with a defined project, with a java interface, and we just created branches to work separately, this was really fast to show our code in the cloud in bitbucket, and we comment each other to just merging the project in the master line.

So yes, the answer is that we review the code ourselfs, by studying the possibilites of what type of implementation we need to use at any moment, thinking about good code, refactor code, etc...

We just make changes in the project in some classes using pair programming and then when we finish we just check in to the remote repository in bitbucket, so after finishing a task, we changes the roles to introduces new code in the project. 

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Repository owner: Mario Benito (alias 1001mario).
Admins: Senmao Ji Ye (alias Senmao).

Both owner or admin can answer you at any moment, if you have any question
or doubt about the project or something, you should talk to them first.