package marbeni_senjiye_Metro;

/**
 * Representa una estacion de metro.
 * 
 * Toda estacion tiene un nombre distinto dentro de la red de metro.
 * 
 * Las estaciones de metro se almacenan en las lineas de las redes de metro,
 * representando las paradas que puede hacer cada una de las lineas de metro.
 * 
 * @author Mario Benito Rodriguez
 * @author Senmao Ji Ye
 * 
 */
public class Estacion {
	private String nombre;

	/**
	 * Crea una estacion con un nombre determinado.
	 * 
	 * Una estacion no puede tener un nombre nulo o un nombre vacio, en el caso
	 * de que se intente crear una estacion con una cadena de caracteres vacia o
	 * bien un valor nulo, se lanzara una excepcion de tipo
	 * IllegalArgumentException.
	 * 
	 * La razon de esto se debe a que no tiene sentido que exista una estacion
	 * en una red de metro con un nombre vacio o nulo.
	 * 
	 * Una vez que se ha creado la estacion, no se puede cambiar de nombre a esa
	 * estacion.
	 * 
	 * @param nombre
	 *            nombre de la estacion
	 * @throws IllegalArgumentException
	 *             Si el nombre es un valor nulo o una cadena vacia
	 */
	public Estacion(String nombre) {
		if (nombre == null)
			throw new IllegalArgumentException(
					"El nombre de la estacion no puede ser un valor nulo");
		if (nombre.equals(""))
			throw new IllegalArgumentException(
					"El nombre de la estacion no puede ser una cadena vacia");
		this.nombre = nombre;
	}

	/**
	 * Devuelve del nombre de la estacion.
	 * 
	 * @return String - nombre de la estacion
	 */
	public String getNombre() {
		return nombre;
	}

	@Override
	public boolean equals(Object objeto) {
		if (objeto == null)
			return false;
		if (getClass() != objeto.getClass())
			return false;
		Estacion estacion = (Estacion) objeto;
		return nombre.equals(estacion.nombre);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Estacion " + nombre;
	}
}
