package marbeni_senjiye_Metro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Representa una linea de metro.
 * 
 * Cada linea esta identificada de manera distinta con un numero y un nombre
 * determinado.
 * 
 * Las lineas tienen un numero de estaciones o paradas por las cuales van
 * pasando el metro determinado.
 * 
 * @author Mario Benito Rodriguez
 * @author Senmao Ji Ye
 * 
 */
public class Linea {
	private int numero;
	private String nombre;
	private ArrayList<Estacion> estaciones;

	/**
	 * Crea una linea con un numero, un nombre y un conjunto de estaciones
	 * asociadas.
	 * 
	 * Toda linea necesita tener por lo menos 2 estaciones para poder ser
	 * creada, ademas, el numero no puede ser un numero negativo ni cero, tiene
	 * que ser un numero positivo y el nombre no puede ser vacio o un valor
	 * nulo.
	 * 
	 * Las estaciones no pueden tener un valor nulo o ser vacias, ademas no
	 * puede haber estaciones iguales dentro de la linea de metro.
	 * 
	 * Se establecen las estaciones extremas de la linea a partir de la primera
	 * y ultima estacion que se pasa como argumento.
	 * 
	 * En el caso de que no se cumplan estas condiciones, se lanzara una
	 * excepcion de tipo IllegalArgumentException en tiempo de ejecucion de
	 * creacion de la linea.
	 * 
	 * @param numero
	 *            Numero de la linea
	 * @param nombre
	 *            Nombre de la linea
	 * @param estaciones
	 *            Array de estaciones de la linea
	 * 
	 * @throws IllegalArgumentException
	 *             Si se pasan menos de dos estaciones, las estaciones son nulas
	 *             o vacias, si existen estaciones iguales, un nombre nulo o
	 *             cadena vacia, un numero negativo o el numero cero
	 */
	public Linea(int numero, String nombre, Estacion[] estaciones) {
		if (estaciones == null)
			throw new IllegalArgumentException(
					"El array de estaciones no puede ser un valor nulo");
		if (estaciones.length < 2)
			throw new IllegalArgumentException(
					"Una linea debe formarse con al menos dos estaciones o mas");
		if (Arrays.asList(estaciones).contains(null)) {
			// La implementacion de la lista permite el valor nulo,
			// esto nos permite saber si existen valores nulos en las
			// estaciones.
			throw new IllegalArgumentException(
					"No puede existir una linea con estaciones nulas");
		}
		if (new HashSet<Estacion>(Arrays.asList(estaciones)).size() != Arrays
				.asList(estaciones).size()) {
			// Utilizamos un conjunto para ver si existen
			// estaciones iguales en el array de estaciones.
			throw new IllegalArgumentException(
					"No puede existir una linea con alguna estacion igual");
		}
		if (numero < 0)
			throw new IllegalArgumentException(
					"Una linea no puede ser identificada con un numero negativo");
		if (numero == 0)
			throw new IllegalArgumentException(
					"Una linea no puede ser identificada con el numero cero");
		if (nombre == null)
			throw new IllegalArgumentException(
					"Una linea no puede ser identificada con un nombre nulo");
		if (nombre.equals(""))
			throw new IllegalArgumentException(
					"Una linea no puede ser identificada con un nombre vacio");
		this.numero = numero;
		this.nombre = nombre;
		this.estaciones = new ArrayList<>();
		for (Estacion estacion : estaciones) {
			this.estaciones.add(estacion);
		}
		// Las estaciones extremos son la primera y ultima del array de
		// estaciones, el resto son declaradas estaciones intermedias.
	}

	/**
	 * Devuelve el numero de la linea de metro.
	 * 
	 * @return int - Numero de la linea
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * Devuelve el nombre de la linea de metro.
	 * 
	 * @return String - Nombre de la linea
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Agrega una estacion intermedia en una posicion especifica de la linea de
	 * metro.
	 * 
	 * Con estacion intermedia nos referimos a una estacion que no es una
	 * estacion extrema, es decir, una estacion que no es la inicial ni la
	 * final.
	 * 
	 * Las posiciones de insercion de la linea son valores que pueden ir desde
	 * la segunda posicion hasta la penultima posicion del conjunto de
	 * estaciones que contiene la linea. Las posiciones extremo: 0 y ultima
	 * posicion, se consideran no validas como inserciones intermedias, ya que
	 * este metodo solamente permite agregar estaciones intermedias.
	 * 
	 * @param estacion
	 *            Estacion que se quiere agregar a la linea
	 * @param posicion
	 *            Posicion en la cual se quiere agregar
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta agregar una estacion existente, una estacion
	 *             nula, si se intenta agregar una estacion en un extremo
	 *             (estacion inicial o final) o en una posicion negativa o fuera
	 *             de rango
	 */
	public void agregarEstacion(Estacion estacion, int posicion) {
		if (estacion == null)
			throw new IllegalArgumentException(
					"No se pueden agregar estaciones nulas a la linea");
		if (estaciones.contains(estacion))
			throw new IllegalArgumentException(
					"La linea ya contiene esta estacion");
		if (posicion == 0 || posicion == estaciones.size() - 1)
			throw new IllegalArgumentException(
					"No se pueden agregar estaciones en los extremos de la linea");
		if (posicion < 0)
			throw new IllegalArgumentException(
					"No se pueden agregar estaciones intermedias en una posicion negativa");
		if (posicion >= estaciones.size())
			throw new IllegalArgumentException(
					"No se pueden agregar estaciones intermedias en posiciones fuera de rango");
		estaciones.add(posicion, estacion);
	}

	/**
	 * Elimina una estacion de la linea.
	 * 
	 * Puede lanzar excepciones dependiendo de como se use y de que tipo de
	 * estaciones se intenten eliminar de la linea de metro.
	 * 
	 * @param estacion
	 *            Estacion que se quiere eliminar de la linea
	 * 
	 * @throws IllegalArgumentException
	 *             Si la estacion no existe en la linea, la estacion es nula, si
	 *             la estacion es la inicial o la final (estaciones extremos), o
	 *             bien si al intentar eliminar la estacion hace que la linea no
	 *             tenga dos estaciones minimas
	 */
	public void eliminarEstacion(Estacion estacion) {
		if (estacion == null)
			throw new IllegalArgumentException(
					"No se pueden eliminar estaciones nulas en una linea");
		if (!estaciones.contains(estacion))
			throw new IllegalArgumentException(
					"No se pueden eliminar estaciones que no existen en una linea");
		if (estaciones.indexOf(estacion) == 0
				|| estaciones.indexOf(estacion) == estaciones.size() - 1)
			throw new IllegalArgumentException(
					"No se pueden eliminar estaciones extremas (inicial o final)");
		estaciones.remove(estacion);
	}

	/**
	 * Indica si dos estaciones estan conectadas o no.
	 * 
	 * El metodo lanza una excepcion cuando una de las estaciones pasadas como
	 * argumento es un valor nulo (tambien puede darse el caso de que sean ambas
	 * nulas), o cuando las dos estaciones son las mismas.
	 * 
	 * No tiene sentido que el metodo compruebe si una estacion esta conectada
	 * con otra dentro de la linea si ambas estaciones son las mismas, ya que
	 * van a estar conectadas si o si porque son la misma estacion.
	 * 
	 * @param estacion1
	 *            Una estacion de la linea
	 * @param estacion2
	 *            Otra estacion de la linea
	 * 
	 * @return boolean - true si la linea conecta las estaciones, false cuando
	 *         no conecta las estaciones
	 * 
	 * @throws IllegalArgumentException
	 *             Si una de las estaciones es nula o ambas estaciones son la
	 *             misma
	 */
	public boolean isConectaEstaciones(Estacion estacion1, Estacion estacion2) {
		if (estacion1 == null || estacion2 == null)
			throw new IllegalArgumentException(
					"Una de las estaciones es un valor nulo");
		if (estacion1.equals(estacion2))
			throw new IllegalArgumentException("Las estaciones son la misma");
		if (estaciones.contains(estacion1) && estaciones.contains(estacion2)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Devuelve las estaciones o paradas que tienen en comun la propia linea con
	 * otra linea pasada que se pasa como argumento.
	 * 
	 * Calcula la correspondencia de ambas lineas y lanza una excepcion cuando
	 * la linea pasada como argumento tiene identificadores iguales (mismo
	 * numero o mismo nombre) que la linea propia, cuando la linea es un valor
	 * nulo, o cuando tiene las mismas estaciones identicas aun siendo lineas
	 * diferentes.
	 * 
	 * En el caso de que la linea2 (la que se pasa como argumento), tenga
	 * diferentes estaciones que la linea propia, se devolvera un conjunto vacio
	 * sin estaciones, esto es la manera de indicar que no existe
	 * correspondencia entre las lineas.
	 * 
	 * @param linea2
	 *            Linea con la que se quiere ver si hay correspondencia
	 * 
	 * @return Estacion[] - Array de paradas comunes
	 * 
	 * @throws IllegalArgumentException
	 *             Si la linea2 tiene el mismo numero que la linea propia, mismo
	 *             nombre que la propia linea, si la linea2 es un valor nulo, si
	 *             la linea2 es la misma linea que la propia, o bien si tiene
	 *             las mismas estaciones siendo lineas distintas
	 */
	public Estacion[] getParadasComunes(Linea linea2) {
		if (linea2 == null)
			throw new IllegalArgumentException("La linea es un valor nulo");
		if (this.equals(linea2))
			throw new IllegalArgumentException("Las lineas son iguales");
		if (numero == linea2.numero)
			throw new IllegalArgumentException(
					"La linea no puede tener el mismo numero");
		if (nombre.equals(linea2.nombre))
			throw new IllegalArgumentException(
					"La linea no puede tener el mismo nombre");
		if (estaciones.equals(linea2.estaciones))
			// La linea tiene nombre y numero diferentes, pero comprobamos
			// que las estaciones son diferentes para continuar!
			throw new IllegalArgumentException(
					"La linea no puede tener las mismas estaciones");
		ArrayList<Estacion> paradasComunes = new ArrayList<>();
		for (Estacion estacion : linea2.estaciones) {
			if (estaciones.contains(estacion)) {
				paradasComunes.add(estacion);
			}
		}
		return paradasComunes.toArray(new Estacion[paradasComunes.size()]);
	}

	/**
	 * Devuelve el numero de paradas existentes entre dos estaciones
	 * pertenecientes a la linea.
	 * 
	 * Se deben indicar estaciones existentes en la linea de metro, asi como
	 * tampoco se pueden pasar estaciones con valor nulo, de lo contrario se
	 * lanzara una excepcion de tipo IllegalArgumentException.
	 * 
	 * No importa realmente el orden de las estaciones (si una va antes o
	 * despues de la otra), de manera que el propio metodo calculara el numero
	 * de paradas en funcion de esas estaciones existentes.
	 * 
	 * @param estacion1
	 *            Primera estacion de la linea
	 * @param estacion2
	 *            Segunda estacion de la linea
	 * 
	 * @return int - Numero de paradas entre ambas estaciones
	 * 
	 * @throws IllegalArgumentException
	 *             Si una de las estaciones no existe dentro del metro, una
	 *             estacion es nula, o bien cuando las dos estaciones son
	 *             iguales
	 */
	public int getNumParadas(Estacion estacion1, Estacion estacion2) {
		if (estacion1 == null || estacion2 == null)
			throw new IllegalArgumentException(
					"Una de las estaciones es un valor nulo");
		if (!estaciones.contains(estacion1) || !estaciones.contains(estacion2))
			throw new IllegalArgumentException(
					"Una de las estaciones no esta en la linea");
		if (estacion1.equals(estacion2))
			throw new IllegalArgumentException("Las estaciones son iguales");
		int posicion1 = estaciones.indexOf(estacion1);
		int posicion2 = estaciones.indexOf(estacion2);
		if (posicion1 < posicion2) {
			return posicion2 - posicion1 - 1;
		} else {
			return posicion1 - posicion2 - 1;
		}
	}

	/**
	 * Devuelve un array de estaciones en orden.
	 * 
	 * @return Estacion[] - Array de estaciones en orden
	 */
	public Estacion[] toArrayEstaciones() {
		return estaciones.toArray(new Estacion[estaciones.size()]);
	}

	/**
	 * Devuelve un array de estaciones de manera inversa.
	 * 
	 * @return Estacion[] - Array inverso de estaciones
	 */
	public Estacion[] toArrayEstacionesInverso() {
		// TODO Auto-generated method stub
		ArrayList<Estacion> estacionesInversas = new ArrayList<>();
		for (int i = estaciones.size() - 1; i >= 0; i--) {
			estacionesInversas.add(estaciones.get(i));
		}
		return estacionesInversas.toArray(new Estacion[estacionesInversas
				.size()]);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((estaciones == null) ? 0 : estaciones.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + numero;
		return result;
	}

	@Override
	public boolean equals(Object objeto) {
		if (objeto == null)
			return false;
		if (getClass() != objeto.getClass())
			return false;
		Linea linea = (Linea) objeto;
		return (numero == linea.numero && nombre.equals(linea.nombre) && estaciones
				.equals(linea.estaciones));
	}

	@Override
	public String toString() {
		return "Linea " + numero + ": " + nombre + " con estaciones: "
				+ estaciones;
	}
}
