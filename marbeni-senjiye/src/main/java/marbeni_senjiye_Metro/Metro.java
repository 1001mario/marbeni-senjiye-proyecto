package marbeni_senjiye_Metro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Representa una red de metro.
 * 
 * Una red de metro contiene las distintas lineas de metro en las cuales se van
 * a realizar paradas especificas.
 * 
 * @author Mario Benito Rodriguez
 * @author Senmao Ji Ye
 * 
 */
public class Metro {

	private ArrayList<Linea> lineas;

	/**
	 * Crea una red de metro con un conjunto de lineas asociadas.
	 * 
	 * Toda red de metro necesita tener por lo menos 2 lineas para poder ser
	 * creada, necesitan ser lineas diferentes, con distintos numeros y nombres,
	 * ademas de ser lineas con un numero de estaciones que son distintas todas
	 * y cada una de ellas.
	 * 
	 * En el caso de que no se cumplan estas condiciones, se lanzara una
	 * excepcion de tipo IllegalArgumentException en tiempo de ejecucion.
	 * 
	 * @param lineas
	 *            Array de lineas que formara parte de la red de metro
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta crear el metro con menos de dos lineas, si las
	 *             lineas son diferentes pero tienen las mismas estaciones
	 *             (todas las estaciones identicas), si las lineas son nulas o
	 *             si no existen, si el array de lineas es nulo, o bien si
	 *             existen lineas iguales ya sea con nombre o numeros iguales
	 */
	public Metro(Linea[] lineas) {
		if (lineas == null) {
			throw new IllegalArgumentException(
					"No puede existir una red de metro nula");
		}
		if (lineas.length < 2) {
			throw new IllegalArgumentException(
					"No puede existir una red de metro con menos de dos lineas");
		}
		if (Arrays.asList(lineas).contains(null)) {
			// La implementacion de la lista permite el valor nulo,
			// esto nos permite saber si existen valores nulos en las
			// lineas.
			throw new IllegalArgumentException(
					"No puede existir una red de metro con lineas nulas");
		}
		ArrayList<String> nombres = new ArrayList<String>();
		ArrayList<Integer> numeros = new ArrayList<Integer>();
		ArrayList<List<Estacion>> estaciones = new ArrayList<List<Estacion>>();
		for (Linea linea : lineas) {
			nombres.add(linea.getNombre());
			numeros.add(linea.getNumero());
			estaciones.add(Arrays.asList(linea.toArrayEstaciones()));
		}
		if (new HashSet<String>(nombres).size() != nombres.size()) {
			throw new IllegalArgumentException(
					"No puede existir una red de lineas con mismo nombre en una red de metro");
		}
		if (new HashSet<Integer>(numeros).size() != numeros.size()) {
			throw new IllegalArgumentException(
					"No puede existir una red de lineas con mismo numero en una red de metro");
		}
		for (int i = 0; i < estaciones.size(); i++) {
			for (int j = i + 1; j < estaciones.size(); j++) {
				if (estaciones.get(i).equals(estaciones.get(j))) {
					throw new IllegalArgumentException(
							"No puede existir una red de lineas con las mismas estaciones en una red de metro");
				}
			}
		}
		this.lineas = new ArrayList<Linea>();
		for (Linea linea : lineas) {
			this.lineas.add(linea);
		}
	}

	/**
	 * Devuelve la linea de metro que tenga un nombre especifico.
	 * 
	 * El metodo realiza una busqueda en la red de metro para encontrar una
	 * linea de metro cuyo nombre sea igual al pasado como argumento.
	 * 
	 * Se lanza una exception en tiempo de ejecucion en el caso de que el nombre
	 * que se pase como argumento sea un valor nulo o una cadena vacia, tambien
	 * cuando no existe una linea que sea identificada con ese nombre
	 * determinado.
	 * 
	 * @param nombre
	 *            Nombre unico de la linea de metro buscada
	 * 
	 * @return Linea - Linea de metro con el nombre determinado
	 * 
	 * @throws IllegalArgumentException
	 *             Si el nombre es una cadena vacia o un valor nulo, o bien si
	 *             no existe una linea con ese nombre dentro de la red de metro
	 */
	public Linea getLinea(String nombre) {
		if (nombre == null) {
			throw new IllegalArgumentException(
					"No puede existir lineas con nombre nulo");
		}
		if (nombre.equals("")) {
			throw new IllegalArgumentException(
					"No puede existir lineas con nombre vacio");
		}

		for (Linea linea : lineas) {
			if (linea.getNombre().equals(nombre))
				return linea;
		}
		throw new IllegalArgumentException(
				"No existe la Linea en la red de metro");
	}

	/**
	 * Devuelve la linea de metro que tenga un numero especifico.
	 * 
	 * El metodo realiza una busqueda en la red de metro para encontrar una
	 * linea de metro cuyo numero sea igual al pasado como argumento.
	 * 
	 * Lanza una excepcion de tipo IllegalArgumentException cuando el numero que
	 * se pasa como argumento es el numero cero o un numero negativo, asi como
	 * si no se encuentra la linea especificada por ese numero.
	 * 
	 * @param numero
	 *            Numero unico de la linea de metro buscada
	 * 
	 * @return Linea - Linea de metro con el numero determinado
	 * 
	 * @throws IllegalArgumentException
	 *             Si el numero es cero o negativo, si no se encuentra una linea
	 *             que es identificada con ese numero
	 */
	public Linea getLinea(int numero) {
		if (numero == 0) {
			throw new IllegalArgumentException(
					"No puede existir lineas con numero 0");
		}
		if (numero < 0) {
			throw new IllegalArgumentException(
					"No existe lineas con numero negativo");
		}
		for (Linea linea : lineas) {
			if (linea.getNumero() == numero)
				return linea;
		}
		throw new IllegalArgumentException(
				"No existe la Linea en la red de metro");
	}

	/**
	 * Agrega una linea a la red de metro.
	 * 
	 * No se permite agregar lineas que tengan el mismo nombre o numero que
	 * alguna de las lineas ya pertenecientes al metro, ademas tampoco
	 * permitimos agregar lineas con el mismo conjunto de estaciones (aunque
	 * sean diferentes), ni lineas que tienen valores nulos, de lo contrario se
	 * lanzara una excepcion de tipo IllegalArgumentException.
	 * 
	 * @param linea
	 *            Linea que se quiere agregar a la red de metro
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta agregar una linea existente (ya sea con nombre,
	 *             numero o estaciones iguales), si la linea es nula o bien si
	 *             la linea contiene estaciones nulas
	 */
	public void agregarLinea(Linea linea) {
		if (linea == null) {
			throw new IllegalArgumentException(
					"No se puede añadir una linea nula a una red de metro");
		}
		if (Arrays.asList(linea.toArrayEstaciones()).contains(null)) {
			throw new IllegalArgumentException(
					"No se puede agregar una linea con estaciones nulas");
		}
		for (Linea l : lineas) {
			if (l.getNumero() == linea.getNumero())
				throw new IllegalArgumentException(
						"Ya existe una linea con el mismo numero en la red de metro");
			if (l.getNombre().equals(linea.getNombre()))
				throw new IllegalArgumentException(
						"Ya existe una linea con el mismo nombre en la red de metro");
			if (Arrays.equals(l.toArrayEstaciones(), linea.toArrayEstaciones()))
				throw new IllegalArgumentException(
						"Ya existe una linea con las mismas estaciones en la red de metro");
		}
		lineas.add(linea);
	}

	/**
	 * Elimina una linea de la red de metro.
	 * 
	 * Se lanza una excepcion de tipo IllegalArgumentException si la linea no se
	 * encuentra en la red de metro (linea no existente), ya sea porque el
	 * numero, nombre o estaciones de la linea indicada no coinciden con ninguna
	 * de las lineas de la red de metro o bien si la linea es un valor nulo.
	 * 
	 * Tambien se lanza la excepcion cuando se intenta eliminar una de las dos
	 * lineas que representan la red de metro, esto se debe a la propia
	 * definicion de la linea: las lineas minimas y requeridas para la red de
	 * metro son dos.
	 * 
	 * @param linea
	 *            Linea que se quiere eliminar de la red de metro.
	 * @throws IllegalArgumentException
	 *             Si la linea no existe en la red de metro, si la linea es nula
	 *             o bien si al intentar eliminar la linea hace que la red de
	 *             metro no tenga dos lineas de metro minimas
	 */
	public void eliminarLinea(Linea linea) {
		if (linea == null) {
			throw new IllegalArgumentException(
					"No se puede eliminar una linea nula a una red de metro");
		}
		if (lineas.size() == 2) {
			throw new IllegalArgumentException(
					"No puede eliminar una linea de una red de metro porque necesita dos lineas minimas");
		}
		if (Arrays.asList(linea.toArrayEstaciones()).contains(null)) {
			throw new IllegalArgumentException(
					"No puede eliminar una red de metro con lineas nulas");
		}
		if (!lineas.contains(linea)) {
			throw new IllegalArgumentException(
					"No existe la linea en la red de metro");
		}
		lineas.remove(linea);
	}

	/**
	 * Devuelve un array de las lineas contenidas en la red de metro.
	 * 
	 * @return Linea[] - Array de lineas
	 */
	public Linea[] toArrayLineas() {
		return lineas.toArray(new Linea[lineas.size()]);
	}

	/**
	 * Devuelve las lineas que pasan por una estacion determinada.
	 * 
	 * Se lanza una excepcion de tipo IllegalArgumentException si la estacion no
	 * se encuentra ninguna de las lineas en la red de metro, o bien la estacion
	 * es un valor nulo.
	 * 
	 * @param estacion
	 *            Estacion de la cual queremos saber las lineas que la recorren.
	 * 
	 * @return Linea[] - Array de lineas que pasan por esa estacion.
	 * 
	 * @throws IllegalArgumentException
	 *             Si la estacion no existe en la red de metro o es un valor
	 *             nulo
	 */
	public Linea[] getLineas(Estacion estacion) {
		if (estacion == null)
			throw new IllegalArgumentException(
					"La estacion buscada no puede ser un valor nulo");
		ArrayList<Linea> lineasEstacion = new ArrayList<Linea>();
		for (Linea linea : lineas) {
			if (Arrays.asList(linea.toArrayEstaciones()).contains(estacion)) {
				lineasEstacion.add(linea);
			}
		}
		if (lineasEstacion.isEmpty())
			throw new IllegalArgumentException(
					"No existe la estacion en la red de metro");
		return lineasEstacion.toArray(new Linea[lineasEstacion.size()]);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lineas == null) ? 0 : lineas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object objeto) {
		if (objeto == null)
			return false;
		if (getClass() != objeto.getClass())
			return false;
		Metro metro = (Metro) objeto;
		return lineas.equals(metro.lineas);
	}

	@Override
	public String toString() {
		return "Metro con lineas: " + lineas;
	}
}
