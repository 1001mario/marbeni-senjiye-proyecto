package marbeni_senjiye_Metro;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LineaTest {

	private Linea linea;

	@Before
	public void setUp() {
		// Creamos una linea valida para la prueba de metodos.
		Estacion[] estaciones = { new Estacion("Atocha"),
				new Estacion("Cuatro Caminos"), new Estacion("Chamartin") };
		linea = new Linea(1, "rojo", estaciones);
	}

	@After
	public void tearDown() {
		linea = null;
	}

	@Test
	public void testCrearLineaValida() {
		Estacion[] estaciones = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Linea linea = new Linea(1, "rojo", estaciones);
		assertEquals(1, linea.getNumero());
		assertEquals("rojo", linea.getNombre());
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertArrayEquals(estaciones, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaInsuficientesEstaciones() {
		Estacion[] estaciones = { new Estacion("Atocha") };
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaEstacionesNull() {
		Estacion[] estaciones = null;
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaSinEstaciones() {
		Estacion[] estaciones = new Estacion[0];
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaEstacionesValorNull() {
		Estacion[] estaciones = new Estacion[3];
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaEstacionesIguales() {
		Estacion[] estaciones = { new Estacion("Atocha"),
				new Estacion("Atocha") };
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNumeroNegativo() {
		Estacion[] estaciones = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Linea linea = new Linea(-1, "rojo", estaciones);
		// No pueden existir lineas negativas.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNumeroCero() {
		Estacion[] estaciones = { new Estacion("Usera"),
				new Estacion("Chamartin") };
		Linea linea = new Linea(0, "rojo", estaciones);
		// Consideramos que no existen lineas "cero".
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNombreVacio() {
		Estacion[] estaciones = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Linea linea = new Linea(1, "", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNombreNull() {
		Estacion[] estaciones = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Linea linea = new Linea(1, null, estaciones);
	}

	@Test
	public void testAgregarEstacionNoExistentePosicionIntermedia() {
		// La linea tiene 3 estaciones, agregamos
		// una nueva estacion en la segunda posicion para la prueba.
		linea.agregarEstacion(new Estacion("Valladolid Campo Grande"), 1);
		Estacion[] estacionesEsperadas = { new Estacion("Atocha"),
				new Estacion("Valladolid Campo Grande"),
				new Estacion("Cuatro Caminos"), new Estacion("Chamartin") };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNull() {
		linea.agregarEstacion(null, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionNegativa() {
		// No se permiten agregar estaciones en posiciones negativas.
		linea.agregarEstacion(new Estacion("Valladolid Campo Grande"), -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionMayorRango() {
		linea.agregarEstacion(new Estacion("Valladolid Campo Grande"), 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionExtremoInicial() {
		linea.agregarEstacion(new Estacion("Valladolid Campo Grande"), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionExtremoFinal() {
		linea.agregarEstacion(new Estacion("Valladolid Campo Grande"), 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionExistente() {
		// No debemos dejar agregar una estacion ya existente en la linea.
		linea.agregarEstacion(new Estacion("Atocha"), 1);
	}

	@Test
	public void testEliminarEstacionExistente() {
		linea.eliminarEstacion(new Estacion("Cuatro Caminos"));
		Estacion[] estacionesEsperadas = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionNoExistente() {
		linea.eliminarEstacion(new Estacion("Valladolid Campo Grande"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionExtremoInicial() {
		linea.eliminarEstacion(new Estacion("Atocha"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionExtremoFinal() {
		linea.eliminarEstacion(new Estacion("Chamartin"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionNull() {
		linea.eliminarEstacion(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionesLineaNoValida() {
		linea.eliminarEstacion(new Estacion("Cuatro Caminos"));
		// Aqui bien.
		linea.eliminarEstacion(new Estacion("Chamartin"));
		// Al eliminar dos estaciones, la linea solamente contiene 1 sola
		// estacion.
		// No podemos permitirlo, va en contra de la especificacion de la linea.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarEstacionesIgualesConectadas() {
		boolean isConectadas = linea.isConectaEstaciones(
				new Estacion("Atocha"), new Estacion("Atocha"));
		// No tiene sentido que intentamos ver si dos estaciones iguales esten
		// conectadas.
	}

	@Test
	public void testConsultarEstacionesConectadas() {
		boolean isConectadas = linea.isConectaEstaciones(new Estacion(
				"Chamartin"), new Estacion("Atocha"));
		assertTrue(isConectadas);
	}

	@Test
	public void testConsultarEstacionesNoConectadas() {
		boolean isConectadas = linea.isConectaEstaciones(new Estacion(
				"Valladolid Campo Grande"), new Estacion("Atocha"));
		assertFalse(isConectadas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarEstacionesNull() {
		linea.isConectaEstaciones(null, new Estacion("Atocha"));
	}

	@Test
	public void testConsultarCorrespondenciaLineasValido() {
		Estacion[] estaciones2 = { new Estacion("Chamartin"),
				new Estacion("Ventas") };
		Linea linea2 = new Linea(2, "verde", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		Estacion[] estacionesEsperadas = { new Estacion("Chamartin") };
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasNull() {
		Estacion[] estacionesObtenidas = linea.getParadasComunes(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasNumeroIgual() {
		Estacion[] estaciones2 = { new Estacion("Atocha"),
				new Estacion("Cuatro Caminos"), new Estacion("Chamartin") };
		Linea linea2 = new Linea(1, "verde", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		// Las lineas deben ser diferentes en cuanto a numero.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasNombreIgual() {
		Estacion[] estaciones2 = { new Estacion("Atocha"),
				new Estacion("Cuatro Caminos"), new Estacion("Chamartin") };
		Linea linea2 = new Linea(2, "rojo", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		// Las lineas deben ser diferentes en cuanto a nombre.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasEstacionesIguales() {
		Estacion[] estaciones2 = { new Estacion("Atocha"),
				new Estacion("Cuatro Caminos"), new Estacion("Chamartin") };
		Linea linea2 = new Linea(2, "verde", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		// Consideramos que dos lineas diferentes pero con identicas estaciones
		// no pueden existir y por eso no tenemos permitido consultar su
		// correspondencia.
	}

	@Test
	public void testParadasLineaEstacionesValidas() {
		int numeroParadas = linea.getNumParadas(new Estacion("Chamartin"),
				new Estacion("Atocha"));
		assertEquals(1, numeroParadas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParadasLineaEstacionesIguales() {
		int numeroParadas = linea.getNumParadas(new Estacion("Chamartin"),
				new Estacion("Chamartin"));
		// No tiene sentido que busquemos el numero de paradas entre dos
		// estaciones iguales.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParadasLineaNull() {
		int numeroParadas = linea.getNumParadas(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParadasLineaEstacionesInvalidas() {
		int numeroParadas = linea.getNumParadas(new Estacion("Chamartin"),
				new Estacion("Valladolid Campo Grande"));
	}

	@Test
	public void testArrayEstacionesLinea() {
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		Estacion[] estacionesEsperadas = { new Estacion("Atocha"),
				new Estacion("Cuatro Caminos"), new Estacion("Chamartin") };
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test
	public void testArrayEstacionesLineaInverso() {
		Estacion[] estacionesObtenidas = linea.toArrayEstacionesInverso();
		Estacion[] estacionesEsperadas = { new Estacion("Chamartin"),
				new Estacion("Cuatro Caminos"), new Estacion("Atocha") };
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}
}
