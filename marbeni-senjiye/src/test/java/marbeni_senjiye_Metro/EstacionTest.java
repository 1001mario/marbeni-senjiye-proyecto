package marbeni_senjiye_Metro;

import static org.junit.Assert.*;

import org.junit.Test;

public class EstacionTest {

	@Test
	public void testCrearEstacionValida() {
		Estacion estacion = new Estacion("Atocha");
		assertEquals("Atocha", estacion.getNombre());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearEstacionNombreVacio() {
		// "" = new String() => Significa lo mismo en Java
		Estacion estacion = new Estacion("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearEstacionNombreNull() {
		Estacion estacion = new Estacion(null);
	}

}
