package marbeni_senjiye_Metro;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MetroTest {

	private Metro metro;

	@Before
	public void setUp() {
		// Preparamos las clases para la prueba de metodos.
		// Creamos un metro valido para ello.
		Linea[] lineas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(2, "verde",
						new Estacion[] { new Estacion("Cuatro Caminos"),
								new Estacion("Ventas") }),
				new Linea(3, "azul", new Estacion[] {
						new Estacion("Valladolid Campo Grande"),
						new Estacion("Las Rosas") }) };
		metro = new Metro(lineas);
	}

	@After
	public void tearDown() {
		metro = null;
	}

	@Test
	public void testCreaMetroValido1() {
		Linea[] lineas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(2, "verde",
						new Estacion[] { new Estacion("Cuatro Caminos"),
								new Estacion("Ventas") }) };
		Metro metro = new Metro(lineas);
		Linea[] lineasObtenidas = metro.toArrayLineas();
		assertArrayEquals(lineas, lineasObtenidas);
	}

	@Test
	public void testCreaMetroValido2() {
		Linea[] lineas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(2, "verde", new Estacion[] {
						new Estacion("Chamartin"), new Estacion("Ventas") }) };
		// las dos lineas de la red de metro pasan por la estacion Chamartin,
		// sin embargo, son diferentes y tienen otras estaciones diferentes.
		Metro metro = new Metro(lineas);
		Linea[] lineasObtenidas = metro.toArrayLineas();
		assertArrayEquals(lineas, lineasObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroUnaLinea() {
		Linea[] lineas = { new Linea(1, "roja", new Estacion[] {
				new Estacion("Atocha"), new Estacion("Chamartin") }) };
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroSinLineas() {
		Linea[] lineas = new Linea[0];
		// Aqui no existen lineas, ya que es un array vacio.
		// No se reserva espacio para para las lineas.
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasNull() {
		Linea[] lineas = null;
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasVacias() {
		Linea[] lineas = new Linea[7];
		// Aqui todas las lineas del array son null.
		// Si se reserva espacio para las 7 en el array.
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasNumerosIguales() {
		Linea[] lineas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(1, "verde",
						new Estacion[] { new Estacion("Cuatro Caminos"),
								new Estacion("Ventas") }) };
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasNombresIguales() {
		Linea[] lineas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(2, "roja",
						new Estacion[] { new Estacion("Cuatro Caminos"),
								new Estacion("Ventas") }) };
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasEstacionesIguales() {
		Linea[] lineas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(3, "verde", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }) };
		// Varias lineas pueden pasar por una misma estacion, pero no
		// puede ser que haya lineas con todas las mismas estaciones iguales.
		// Consideramos que esta situacion no se deberia de dar.
		Metro metro = new Metro(lineas);
	}

	@Test
	public void testAgregarLineaNoExistente() {
		metro.agregarLinea(new Linea(4, "amarilla", new Estacion[] {
				new Estacion("Retiro"), new Estacion("Usera") }));
		Linea[] lineasObtenidas = metro.toArrayLineas();
		Linea[] lineasEsperadas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(2, "verde",
						new Estacion[] { new Estacion("Cuatro Caminos"),
								new Estacion("Ventas") }),
				new Linea(3, "azul", new Estacion[] {
						new Estacion("Valladolid Campo Grande"),
						new Estacion("Las Rosas") }),
				new Linea(4, "amarilla", new Estacion[] {
						new Estacion("Retiro"), new Estacion("Usera") }) };
		assertArrayEquals(lineasEsperadas, lineasObtenidas);
		// La red de metro contendra 4 lineas: roja, verde, azul y amarilla.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaNombreExistente() {
		metro.agregarLinea(new Linea(4, "roja", new Estacion[] {
				new Estacion("Retiro"), new Estacion("Usera") }));
		// Ya existe una linea con ese nombre.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaNumeroExistente() {
		metro.agregarLinea(new Linea(1, "amarilla", new Estacion[] {
				new Estacion("Retiro"), new Estacion("Usera") }));
		// Ya existe una linea con ese numero.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaEstacionesExistente() {
		metro.agregarLinea(new Linea(4, "amarilla", new Estacion[] {
				new Estacion("Atocha"), new Estacion("Chamartin") }));
		// Ya existe una linea con esas estaciones.
		// Recordamos que no dejamos agregar lineas diferentes con estaciones
		// iguales.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaNull() {
		metro.agregarLinea(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaNumeroNoExistente() {
		metro.eliminarLinea(new Linea(5, "verde", new Estacion[] {
				new Estacion("Cuatro Caminos"), new Estacion("Ventas") }));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaNombreNoExistente() {
		metro.eliminarLinea(new Linea(2, "violeta", new Estacion[] {
				new Estacion("Cuatro Caminos"), new Estacion("Ventas") }));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaEstacionesNoExistente() {
		metro.eliminarLinea(new Linea(2, "verde", new Estacion[] {
				new Estacion("Atocha"), new Estacion("Ventas") }));
	}

	@Test
	public void testEliminarLineaExistente() {
		metro.eliminarLinea(new Linea(1, "roja", new Estacion[] {
				new Estacion("Atocha"), new Estacion("Chamartin") }));
		Linea[] lineasEsperadas = {
				new Linea(2, "verde",
						new Estacion[] { new Estacion("Cuatro Caminos"),
								new Estacion("Ventas") }),
				new Linea(3, "azul", new Estacion[] {
						new Estacion("Valladolid Campo Grande"),
						new Estacion("Las Rosas") }) };
		Linea[] lineasObtenidas = metro.toArrayLineas();
		assertArrayEquals(lineasEsperadas, lineasObtenidas);
		// Al eliminar la linea roja nos quedan 2 lineas: verde y azul.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineasMetroNoValido() {
		metro.eliminarLinea(new Linea(1, "roja", new Estacion[] {
				new Estacion("Atocha"), new Estacion("Chamartin") }));
		// Al eliminar una linea existente nos quedan 2 lineas: verde y azul.

		metro.eliminarLinea(new Linea(3, "azul", new Estacion[] {
				new Estacion("Valladolid Campo Grande"),
				new Estacion("Las Rosas") }));
		// Al intentar borrar la linea 3 nos quedaria solamente una sola linea
		// en la red de metro, no se debe dejar borrar esta ultima!
		// Debido a la propia definicion de la red de metro (2 lineas minimas).
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaNull() {
		metro.eliminarLinea(null);
	}

	@Test
	public void testObtenerLineaConNumeroExistente() {
		Linea linea = metro.getLinea(1);
		Estacion[] estacionesEsperadas = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertEquals(1, linea.getNumero());
		assertEquals("roja", linea.getNombre());
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
		// Nos proporciona la linea con numero = 1 y con nombre = "roja",
		// ya que no puede haber dos lineas repetidas dentro de una red de
		// metro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNumeroNoExistente() {
		Linea linea = metro.getLinea(4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNumeroCero() {
		Linea linea = metro.getLinea(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNumeroNegativa() {
		Linea linea = metro.getLinea(-1);
	}

	@Test
	public void testObtenerLineaConNombreExistente() {
		Linea linea = metro.getLinea("roja");
		Estacion[] estacionesEsperadas = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertEquals(1, linea.getNumero());
		assertEquals("roja", linea.getNombre());
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
		// Nos proporciona la linea con numero = 1 y con nombre = "roja",
		// ya que no puede haber dos lineas repetidas dentro de una red de
		// metro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNombreNoExistente() {
		Linea linea = metro.getLinea("violeta");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNombreVacio() {
		Linea linea = metro.getLinea("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNombreNull() {
		Linea linea = metro.getLinea(null);
	}

	@Test
	public void testObtenerArrayMetro() {
		Linea[] lineasObtenidas = metro.toArrayLineas();
		Linea[] lineasEsperadas = {
				new Linea(1, "roja", new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }),
				new Linea(2, "verde",
						new Estacion[] { new Estacion("Cuatro Caminos"),
								new Estacion("Ventas") }),
				new Linea(3, "azul", new Estacion[] {
						new Estacion("Valladolid Campo Grande"),
						new Estacion("Las Rosas") }) };
		assertArrayEquals(lineasEsperadas, lineasObtenidas);
		// No existen problemas a la hora de proporcionar un array de lineas
		// de un metro, ya que todo metro tiene al menos dos lineas.
	}

	@Test
	public void testLineasMetroEstacionValida() {
		Linea[] lineasObtenidas = metro.getLineas(new Estacion("Atocha"));
		Linea[] lineasEsperadas = { new Linea(1, "roja", new Estacion[] {
				new Estacion("Atocha"), new Estacion("Chamartin") }) };
		assertArrayEquals(lineasEsperadas, lineasObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testLineasMetroEstacionNoValida() {
		Linea[] lineas = metro.getLineas(new Estacion("Usera"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testLineasMetroEstacionNull() {
		Linea[] lineas = metro.getLineas(null);
	}
}
