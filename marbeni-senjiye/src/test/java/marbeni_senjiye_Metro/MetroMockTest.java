package marbeni_senjiye_Metro;

import static org.easymock.EasyMock.*;
import org.easymock.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MetroMockTest {

	private Metro metro;
	@Mock
	private Linea lineaA;
	@Mock
	private Linea lineaB;
	@Mock
	private Linea lineaC;

	@Before
	public void setUp() {
		lineaA = createMock(Linea.class);
		lineaB = createMock(Linea.class);
		lineaC = createMock(Linea.class);
		expect(lineaA.getNombre()).andReturn("roja").anyTimes();
		expect(lineaA.getNumero()).andReturn(1).anyTimes();
		expect(lineaA.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }).anyTimes();
		expect(lineaB.getNombre()).andReturn("verde").anyTimes();
		expect(lineaB.getNumero()).andReturn(2).anyTimes();
		expect(lineaB.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Cuatro Caminos"),
						new Estacion("Ventas") }).anyTimes();
		expect(lineaC.getNombre()).andReturn("azul").anyTimes();
		expect(lineaC.getNumero()).andReturn(3).anyTimes();
		expect(lineaC.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Valladolid Campo Grande"),
						new Estacion("Las Rosas") }).anyTimes();
		replay(lineaA);
		replay(lineaB);
		replay(lineaC);
		metro = new Metro(new Linea[] { lineaA, lineaB, lineaC });
		verify(lineaA);
		verify(lineaB);
		verify(lineaC);
	}

	@After
	public void tearDown() {
		metro = null;
		lineaA = null;
		lineaB = null;
		lineaC = null;
	}

	@Test
	public void testCreaMetroValido1() {
		Linea[] lineas = { lineaA, lineaB };
		Metro metro = new Metro(lineas);
		Linea[] lineasObtenidas = metro.toArrayLineas();
		assertArrayEquals(lineas, lineasObtenidas);
	}

	@Test
	public void testCreaMetroValido2() {
		Linea[] lineas = new Linea[] { lineaA, lineaB };
		Metro metro = new Metro(lineas);
		Linea[] lineasObtenidas = metro.toArrayLineas();
		assertArrayEquals(lineas, lineasObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroUnaLinea() {
		Linea[] lineas = { lineaA };
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroSinLineas() {
		Linea[] lineas = new Linea[0];
		// Aqui no existen lineas, ya que es un array vacio.
		// No se reserva espacio para para las lineas.
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasNull() {
		Linea[] lineas = null;
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasVacias() {
		Linea[] lineas = new Linea[7];
		// Aqui todas las lineas del array son null.
		// Si se reserva espacio para las 7 en el array.
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasNumerosIguales() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("verde").anyTimes();
		expect(lineaD.getNumero()).andReturn(1).anyTimes();
		expect(lineaD.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Cuatro Caminos"),
						new Estacion("Ventas") }).anyTimes();
		replay(lineaD);
		Linea[] lineas = { lineaA, lineaD };
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasNombresIguales() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("roja").anyTimes();
		expect(lineaD.getNumero()).andReturn(2).anyTimes();
		expect(lineaD.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Cuatro Caminos"),
						new Estacion("Ventas") }).anyTimes();
		replay(lineaD);
		Linea[] lineas = { lineaA, lineaD };
		Metro metro = new Metro(lineas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreaMetroLineasEstacionesIguales() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("verde").anyTimes();
		expect(lineaD.getNumero()).andReturn(3).anyTimes();
		expect(lineaD.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }).anyTimes();
		replay(lineaD);
		Linea[] lineas = { lineaA, lineaD };
		// Varias lineas pueden pasar por una misma estacion, pero no
		// puede ser que haya lineas con todas las mismas estaciones iguales.
		// Consideramos que esta situacion no se deberia de dar.
		Metro metro = new Metro(lineas);
	}

	@Test
	public void testAgregarLineaNoExistente() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("Amarillo").anyTimes();
		expect(lineaD.getNumero()).andReturn(4).anyTimes();
		expect(lineaD.toArrayEstaciones())
				.andReturn(
						new Estacion[] { new Estacion("Retiro"),
								new Estacion("Usera") }).anyTimes();
		replay(lineaD);
		metro.agregarLinea(lineaD);
		Linea[] lineasObtenidas = metro.toArrayLineas();
		Linea[] lineasEsperadas = { lineaA, lineaB, lineaC, lineaD };
		assertArrayEquals(lineasEsperadas, lineasObtenidas);

		// La red de metro contendra 4 lineas: roja, verde, azul y amarilla.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaNombreExistente() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("roja").anyTimes();
		expect(lineaD.getNumero()).andReturn(4).anyTimes();
		expect(lineaD.toArrayEstaciones())
				.andReturn(
						new Estacion[] { new Estacion("Retiro"),
								new Estacion("Usera") }).anyTimes();
		replay(lineaD);
		metro.agregarLinea(lineaD);
		// Ya existe una linea con ese nombre.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaNumeroExistente() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("amarilla").anyTimes();
		expect(lineaD.getNumero()).andReturn(1).anyTimes();
		expect(lineaD.toArrayEstaciones())
				.andReturn(
						new Estacion[] { new Estacion("Retiro"),
								new Estacion("Usera") }).anyTimes();
		replay(lineaD);
		metro.agregarLinea(lineaD);
		// Ya existe una linea con ese numero.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaEstacionesExistente() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("amarilla").anyTimes();
		expect(lineaD.getNumero()).andReturn(4).anyTimes();
		expect(lineaD.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Atocha"),
						new Estacion("Chamartin") }).anyTimes();
		replay(lineaD);
		metro.agregarLinea(lineaD);
		// Ya existe una linea con esas estaciones.
		// Recordamos que no dejamos agregar lineas diferentes con estaciones
		// iguales.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarLineaNull() {
		metro.agregarLinea(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaNumeroNoExistente() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("verde").anyTimes();
		expect(lineaD.getNumero()).andReturn(5).anyTimes();
		expect(lineaD.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Cuatro Camino"),
						new Estacion("Ventas") }).anyTimes();
		replay(lineaD);
		metro.eliminarLinea(lineaD);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaNombreNoExistente() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("violeta").anyTimes();
		expect(lineaD.getNumero()).andReturn(2).anyTimes();
		expect(lineaD.toArrayEstaciones()).andReturn(
				new Estacion[] { new Estacion("Cuatro Camino"),
						new Estacion("Ventas") }).anyTimes();
		replay(lineaD);
		metro.eliminarLinea(lineaD);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaEstacionesNoExistente() {
		Linea lineaD = createMock(Linea.class);
		expect(lineaD.getNombre()).andReturn("verde").anyTimes();
		expect(lineaD.getNumero()).andReturn(2).anyTimes();
		expect(lineaD.toArrayEstaciones())
				.andReturn(
						new Estacion[] { new Estacion("Atocha"),
								new Estacion("Ventas") }).anyTimes();
		replay(lineaD);
		metro.eliminarLinea(lineaD);
	}

	@Test
	public void testEliminarLineaExistente() {

		metro.eliminarLinea(lineaA);
		Linea[] lineasEsperadas = { lineaB, lineaC };
		Linea[] lineasObtenidas = metro.toArrayLineas();
		assertArrayEquals(lineasEsperadas, lineasObtenidas);
		// Al eliminar la linea roja nos quedan 2 lineas: verde y azul.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineasMetroNoValido() {

		metro.eliminarLinea(lineaA);
		// Al eliminar una linea existente nos quedan 2 lineas: verde y azul.

		metro.eliminarLinea(lineaC);
		// Al intentar borrar la linea 3 nos quedaria solamente una sola linea
		// en la red de metro, no se debe dejar borrar esta ultima!
		// Debido a la propia definicion de la red de metro (2 lineas minimas).
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarLineaNull() {
		metro.eliminarLinea(null);
	}

	@Test
	public void testObtenerLineaConNumeroExistente() {
		Linea linea = metro.getLinea(1);
		Estacion[] estacionesEsperadas = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertEquals(1, linea.getNumero());
		assertEquals("roja", linea.getNombre());
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
		// Nos proporciona la linea con numero = 1 y con nombre = "roja",
		// ya que no puede haber dos lineas repetidas dentro de una red de
		// metro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNumeroNoExistente() {
		Linea linea = metro.getLinea(4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNumeroCero() {
		Linea linea = metro.getLinea(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNumeroNegativa() {
		Linea linea = metro.getLinea(-1);
	}

	@Test
	public void testObtenerLineaConNombreExistente() {
		Linea linea = metro.getLinea("roja");
		Estacion[] estacionesEsperadas = { new Estacion("Atocha"),
				new Estacion("Chamartin") };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertEquals(1, linea.getNumero());
		assertEquals("roja", linea.getNombre());
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
		// Nos proporciona la linea con numero = 1 y con nombre = "roja",
		// ya que no puede haber dos lineas repetidas dentro de una red de
		// metro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNombreNoExistente() {
		Linea linea = metro.getLinea("violeta");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNombreVacio() {
		Linea linea = metro.getLinea("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtenerLineaConNombreNull() {
		Linea linea = metro.getLinea(null);
	}

	@Test
	public void testObtenerArrayMetro() {
		Linea[] lineasObtenidas = metro.toArrayLineas();
		Linea[] lineasEsperadas = { lineaA, lineaB, lineaC };
		assertArrayEquals(lineasEsperadas, lineasObtenidas);
		// No existen problemas a la hora de proporcionar un array de lineas
		// de un metro, ya que todo metro tiene al menos dos lineas.
	}

	@Test
	public void testLineasMetroEstacionValida() {
		Linea[] lineasObtenidas = metro.getLineas(new Estacion("Atocha"));
		Linea[] lineasEsperadas = { lineaA };
		assertArrayEquals(lineasEsperadas, lineasObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testLineasMetroEstacionNoValida() {
		Linea[] lineas = metro.getLineas(new Estacion("Usera"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testLineasMetroEstacionNull() {
		Linea[] lineas = metro.getLineas(null);
	}

}
