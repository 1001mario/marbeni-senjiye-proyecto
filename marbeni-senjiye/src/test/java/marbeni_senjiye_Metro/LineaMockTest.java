package marbeni_senjiye_Metro;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;

import org.easymock.*;

public class LineaMockTest {

	private Linea linea;
	@Mock
	private Estacion estacion1;
	@Mock
	private Estacion estacion2;
	@Mock
	private Estacion estacion3;

	@Before
	public void setUp() throws Exception {
		estacion1 = createMock(Estacion.class);
		estacion2 = createMock(Estacion.class);
		estacion3 = createMock(Estacion.class);
		expect(estacion1.getNombre()).andReturn("Atocha").anyTimes();
		expect(estacion2.getNombre()).andReturn("Cuatro Caminos").anyTimes();
		expect(estacion3.getNombre()).andReturn("Chamartin").anyTimes();
		replay(estacion1);
		replay(estacion2);
		replay(estacion3);
		linea = new Linea(1, "rojo", new Estacion[] { estacion1, estacion2,
				estacion3 });
	}

	@After
	public void tearDown() throws Exception {
		linea = null;
	}

	@Test
	public void testCrearLineaValida() {
		Estacion[] estaciones = { estacion1, estacion2 };
		Linea linea = new Linea(1, "rojo", estaciones);
		assertEquals(1, linea.getNumero());
		assertEquals("rojo", linea.getNombre());
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertArrayEquals(estaciones, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaInsuficientesEstaciones() {
		Linea linea = new Linea(1, "rojo", new Estacion[] { estacion1 });
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaEstacionesNull() {
		Estacion[] estaciones = null;
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaSinEstaciones() {
		Estacion[] estaciones = new Estacion[0];
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaEstacionesValorNull() {
		Estacion[] estaciones = new Estacion[3];
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaEstacionesIguales() {
		Estacion[] estaciones = { estacion1, estacion1 };
		Linea linea = new Linea(1, "rojo", estaciones);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNumeroNegativo() {
		Estacion[] estaciones = { estacion1, estacion2 };
		Linea linea = new Linea(-1, "rojo", estaciones);
		// No pueden existir lineas negativas.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNumeroCero() {
		Estacion[] estaciones = { estacion1, estacion2 };
		Linea linea = new Linea(0, "rojo", estaciones);
		// Consideramos que no pueden existir lineas "cero".
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNombreVacio() {
		Estacion[] estaciones = { estacion1, estacion2 };
		Linea linea = new Linea(1, "", estaciones);
		// No permitimos que la linea tenga de nombre una cadena vacia
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearLineaNombreNull() {
		Linea linea = new Linea(1, null,
				new Estacion[] { estacion1, estacion2 });
	}

	@Test
	public void testAgregarEstacionNoExistentePosicionIntermedia() {
		// La linea tiene 3 estaciones, agregamos
		// una nueva estacion en la segunda posicion para la prueba.
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		linea.agregarEstacion(estacionNueva, 1);
		Estacion[] estacionesEsperadas = { estacion1, estacionNueva, estacion2,
				estacion3 };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNull() {
		linea.agregarEstacion(null, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionNegativa() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		// No se permiten agregar estaciones en posiciones negativas.
		linea.agregarEstacion(estacionNueva, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionMayorRango() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		linea.agregarEstacion(estacionNueva, 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionExtremoInicial() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		linea.agregarEstacion(estacionNueva, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionNoExistentePosicionExtremoFinal() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		linea.agregarEstacion(estacionNueva, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarEstacionExistente() {
		// No debemos dejar agregar una estacion ya existente en la linea.
		linea.agregarEstacion(estacion1, 1);
	}

	@Test
	public void testEliminarEstacionExistente() {
		linea.eliminarEstacion(estacion2);
		Estacion[] estacionesEsperadas = { estacion1, estacion3 };
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionNoExistente() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		linea.eliminarEstacion(estacionNueva);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionExtremoInicial() {
		linea.eliminarEstacion(estacion1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionExtremoFinal() {
		linea.eliminarEstacion(estacion3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionNull() {
		linea.eliminarEstacion(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarEstacionesLineaNoValida() {
		linea.eliminarEstacion(estacion2);
		// Aqui bien.
		linea.eliminarEstacion(estacion3);
		// No podemos permitir borrar esta estacion.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarEstacionesIgualesConectadas() {
		boolean isConectadas = linea.isConectaEstaciones(estacion1, estacion1);
		// No tiene sentido que intentamos ver si dos estaciones iguales esten
		// conectadas.
	}

	@Test
	public void testConsultarEstacionesConectadas() {
		boolean isConectadas = linea.isConectaEstaciones(estacion3, estacion1);
		assertTrue(isConectadas);
	}

	@Test
	public void testConsultarEstacionesNoConectadas() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		boolean isConectadas = linea.isConectaEstaciones(estacionNueva,
				estacion1);
		assertFalse(isConectadas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarEstacionesNull() {
		linea.isConectaEstaciones(null, estacion1);
	}

	@Test
	public void testConsultarCorrespondenciaLineasValido() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Ventas").anyTimes();
		replay(estacionNueva);
		Estacion[] estaciones2 = { estacion3, estacionNueva };
		Linea linea2 = new Linea(2, "verde", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		Estacion[] estacionesEsperadas = { estacion3 };
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasNull() {
		Estacion[] estacionesObtenidas = linea.getParadasComunes(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasNumeroIgual() {
		Estacion[] estaciones2 = { estacion1, estacion2, estacion3 };
		Linea linea2 = new Linea(1, "verde", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		// Las lineas deben ser diferentes en cuanto a numero.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasNombreIgual() {
		Estacion[] estaciones2 = { estacion1, estacion2, estacion3 };
		Linea linea2 = new Linea(2, "rojo", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		// Las lineas deben ser diferentes en cuanto a nombre.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultarCorrespondenciaLineasEstacionesIguales() {
		Estacion[] estaciones2 = { estacion1, estacion2, estacion3 };
		Linea linea2 = new Linea(2, "verde", estaciones2);
		Estacion[] estacionesObtenidas = linea.getParadasComunes(linea2);
		// Consideramos que dos lineas diferentes pero con identicas estaciones
		// no pueden existir y por eso no tenemos permitido consultar su
		// correspondencia.
	}

	@Test
	public void testParadasLineaEstacionesValidas() {
		int numeroParadas = linea.getNumParadas(estacion3, estacion1);
		assertEquals(1, numeroParadas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParadasLineaEstacionesIguales() {
		int numeroParadas = linea.getNumParadas(estacion3, estacion3);
		// No tiene sentido que busquemos el numero de paradas entre dos
		// estaciones iguales.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParadasLineaNull() {
		int numeroParadas = linea.getNumParadas(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParadasLineaEstacionesInvalidas() {
		Estacion estacionNueva = createMock(Estacion.class);
		expect(estacionNueva.getNombre()).andReturn("Valladolid Campo Grande")
				.anyTimes();
		replay(estacionNueva);
		int numeroParadas = linea.getNumParadas(estacion3, estacionNueva);
	}

	@Test
	public void testArrayEstacionesLinea() {
		Estacion[] estacionesObtenidas = linea.toArrayEstaciones();
		Estacion[] estacionesEsperadas = { estacion1, estacion2, estacion3 };
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}

	@Test
	public void testArrayEstacionesLineaInverso() {
		Estacion[] estacionesObtenidas = linea.toArrayEstacionesInverso();
		Estacion[] estacionesEsperadas = { estacion3, estacion2, estacion1 };
		assertArrayEquals(estacionesEsperadas, estacionesObtenidas);
	}
}
