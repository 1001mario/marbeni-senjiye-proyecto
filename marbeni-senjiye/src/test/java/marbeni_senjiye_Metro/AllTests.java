package marbeni_senjiye_Metro;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ EstacionTest.class, LineaTest.class, MetroTest.class,
		MetroMockTest.class, LineaMockTest.class })
public class AllTests {

}
